// Copyright Epic Games, Inc. All Rights Reserved.

#include "hw22GameMode.h"
#include "hw22PlayerController.h"
#include "../Character/hw22Character.h"
#include "UObject/ConstructorHelpers.h"

Ahw22GameMode::Ahw22GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = Ahw22PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character.BP_Character_C"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	
}