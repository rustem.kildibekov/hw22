// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "hw22GameMode.generated.h"

UCLASS(minimalapi)
class Ahw22GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ahw22GameMode();
};



