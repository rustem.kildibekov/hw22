// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <CoreMinimal.h>
#include <Kismet/BlueprintFunctionLibrary.h>
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    AimNormal_state UMETA(DisplayName = "Aim Normal State"),
    AimWalk_state UMETA(DisplayName = "Aim Walk State"),
    Walk_state UMETA(DisplayName = "Walk State"),
    FastWalk_state UMETA(DisplayName = "Fast Walk State"),
    Run_state UMETA(DisplayName = "Run State"),

};


USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()
    UPROPERTY(EDitAnyWhere, BlueprintReadWrite, Category = "Movement")
    float AimWalkSpeed = 100.0f;
    UPROPERTY(EDitAnyWhere, BlueprintReadWrite, Category = "Movement")
    float AimNormalSpeed = 200.0f;
    UPROPERTY(EDitAnyWhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed = 300.0f;
    UPROPERTY(EDitAnyWhere, BlueprintReadWrite, Category = "Movement")
    float FastWalkSpeed = 400.0f;
    UPROPERTY(EDitAnyWhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 700.0f;
};

UCLASS()
class HW22_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};