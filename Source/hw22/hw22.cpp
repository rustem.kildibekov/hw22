// Copyright Epic Games, Inc. All Rights Reserved.

#include "hw22.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, hw22, "hw22" );

DEFINE_LOG_CATEGORY(Loghw22)
 