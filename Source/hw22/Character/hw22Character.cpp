// Copyright Epic Games, Inc. All Rights Reserved.

#include "hw22Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"


Ahw22Character::Ahw22Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void Ahw22Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void Ahw22Character::SetupPlayerInputComponent(UInputComponent* _InputComponent)
{
	Super::SetupPlayerInputComponent(_InputComponent);
	
	_InputComponent->BindAxis(TEXT("MoveForward"), this, &Ahw22Character::InputAxisX);
	_InputComponent->BindAxis(TEXT("MoveRight"), this, &Ahw22Character::InputAxisY);
}

void Ahw22Character::InputAxisX(float Value)
{
	AxisX = Value;
}

void Ahw22Character::InputAxisY(float Value)
{
	AxisY = Value;
}

void Ahw22Character::MovementTick(float DeltaTime)
{
	//UE_LOG(LogTemp, Warning, TEXT("AxisX %f,  AxisY  %f") , AxisX, AxisY);
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	FRotator direction = FVector(AxisX, AxisY, 0.0f).ToOrientationRotator();
	if (abs(abs(direction.Yaw) - abs(this->GetActorRotation().Yaw)) < 30)
		forwardMoving = true;
	else 
		forwardMoving = false;
	

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController) {
		FHitResult resultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, resultHit);
		FRotator newRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), resultHit.Location);
		SetActorRotation(FQuat(FRotator(0.0f, newRotator.Yaw, 0.0f)));

		
	}
}

void Ahw22Character::characterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::AimWalk_state:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::AimNormal_state:
		ResSpeed = MovementInfo.AimNormalSpeed;
		break;
	case EMovementState::Walk_state:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::FastWalk_state:
		ResSpeed = MovementInfo.FastWalkSpeed;
		break;
	case EMovementState::Run_state:
		if (forwardMoving)
			ResSpeed = MovementInfo.RunSpeed;
		else 
			ResSpeed = MovementInfo.FastWalkSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void Ahw22Character::ChangeMovementState()
{
	if (AimEnabled) {
		if (WalkEnabled) MovementState = EMovementState::AimWalk_state;
		else MovementState = EMovementState::AimNormal_state;
	}
	else if ((RunEnabled && WalkEnabled) || !RunEnabled && !WalkEnabled) {
		MovementState = EMovementState::FastWalk_state;
	} 
	else if (WalkEnabled) {
		MovementState = EMovementState::Walk_state;
	} 
	else if (RunEnabled) {
		MovementState = EMovementState::Run_state;
	}

	characterUpdate();
}
